# Money from Ukraine

As shown in this email, Hunter was reciving about 1 million a year from Burisma, plus 30% of the equity (10 held by him for Joe Biden)

![burisma email](https://images-prod.misbar.com/articlebody/investigation_73ib3v1p2.jpg)

Joe Biden lied when he said he had nothing to do with his son's business in Ukraine, in fact he was also caught on tape bragging about forcing Ukraine to fire the prosecutor who was investigating Burisma's illicit operations.

<iframe width="560" height="315" src="https://www.youtube.com/embed/UXA--dj2-CY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# The full story

[Burisma was created in 2006 by Mykola Zlochevsky](https://www.amazon.com/Profiles-Corruption-Peter-Schweizer/dp/006289790X), the corrupt natural resources minister of Ukraine who conveniently gave himself licenses to develop abundant natural gas fields, turning Burisma into the top Ukrainian natural gas producer.

On April 16, 2014, Devon Archer made a private visit to the White House for a meeting with Joe Biden. The day prior, Burisma had deposited more than $112,000 into a Rosemont bank account marked “C/O Devon Archer.” Six days later, on April 22, it was announced that Archer was joining the board of directors of Burisma. In short succession, on May 13, it was announced that Hunter Biden would join the Burisma board, too. Neither one had any background or experience in the energy sector. 

We now know based on financial records that each man has been paid $1 million a year by Burisma. 

Burisma founder Zlochevsky was in legal crosshairs when Biden and Archer joined the company board. 
By February 2016, Ukrainian authorities seized Zlochevsky’s property on suspicion that he was engaged in “illicit enrichment.” 
Zlochevsky found himself on Ukraine’s wanted list and fled the country. 
The Ukrainian prosecutor general’s office seized Burisma’s gas wells. Tax authorities began investigating him for suspicion of tax evasion. 
Hunter Biden used his contacts in Washington to help Zlochevsky in his corruption case.

Under pressure from Joe Biden, Ukrainian officials fired the Ukrainian prosecutor who was investigating Burisma. 
Joe Biden later bragged that he had the prosecutor fired by threatening to withhold one billion dollars in U.S. aid assistance to Ukraine.

On January 12, 2017, four days before Biden arrived in Ukraine, Burisma announced that the Ukrainian government prosecutors had ended the criminal investigations into the company and its founder Zlochevsky. 
