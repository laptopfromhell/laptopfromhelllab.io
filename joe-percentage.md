# 50 percent Joe

Joe Biden has been taking a [50% cut](https://twitter.com/RudyGiuliani/status/1316804040857137160) of the money sent to Hunter:

![Unlike pop I wont make you give me half your salary](https://pbs.twimg.com/media/EkY6N3gXsAMWRvb?format=jpg&name=medium)

In some cases, Hunter has also held shares of companies in lieu of Joe, "the big guy", to keep Joe Biden's name officially out of it (ex business partner has [confirmed the "big guy" is Joe Biden](https://nypost.com/2020/10/22/hunter-biden-ex-business-partner-told-dont-mention-joe-in-text/))

![10 by H for the Big Guy](https://nypost.com/wp-content/uploads/sites/2/2020/10/Biden-Expectations-email-graphic.jpg?quality=90&strip=all&w=1286)
