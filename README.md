# https://laptopfromhell.github.io

# How to contribute

Click on ["issues"](https://github.com/laptopfromhell/laptopfromhell.github.io/issues) and open a new issue descibing the information you want added or corrected. If you want to become an editor yourself, mention that and we will add you as a collaborator. If you are a more advanced Github user you can also fork the repository and then run a pull request.

# How to link

If you are linking the website on social media, preferably use a link shortener rather than linking to https://laptopfromhell.github.io/ directly. For example:

- https://bit.ly/3mtLCPf
- https://bit.ly/35EvweX
- https://bit.ly/2HFUiTC
- https://shorturl.at/lswGO
- https://shorturl.at/lyzI5
- https://cutt.ly/SgERGpt
- https://tinyurl.com/y6roumut
- https://shorturl.me/8JXGE

# Privacy

If you want to stay fully anonymous while contributing, download [Tor Browser](https://www.torproject.org/download/) and register a Github account from Tor Browser using a temporary email provider (eg. https://tempr.email/ )
