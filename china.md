# Money from china

## 1 Billion investment

In December 2013 Hunter flew aboard Air Force Two to China, accompanying his father on an official visit where Joe met with Hunter’s Chinese partners.
Ten days later, Hunter’s private equity firm "Rosemont Seneca Partners" received a [$1 billion investment from the state-owned Bank of China](https://nypost.com/2020/10/15/emails-reveal-how-hunter-biden-tried-to-cash-in-big-with-chinese-firm/).

## 10 million a year for "introductions"

This email from Hunter's laptop shows a deal where Hunter is to be paid 10 million a year by Chinese billionaire Ye Jianming for "introductions", or 50% of profits in the future venture. 
Ye Jianming is the billionaire Chairman of Chinese energy company CEFC.

![10 million a year](https://nypost.com/wp-content/uploads/sites/2/2020/10/Biden-LUCRATIVE-ARRANGEMENT.jpg?quality=90&strip=all&w=915)

Later on, Ye Jianming (worth "323 billion dollars") faced bribing charges and disappeared,
a recording has surfaced of Hunter Biden being very pissed about this:

<video src="https://i.rmbl.ws/s8/2/f/U/J/f/fUJfb.caa.1.mp4?u=0&amp;b=1" poster="https://i.rmbl.ws/s8/6/f/U/J/f/fUJfb.OvCc.1.jpg" controls></video>


## Additional info

Hunter met with Chinese tycoon Ye Jianming, the chairman of energy company CEFC, in a Miami hotel room and the pair discussed American infrastructure and energy deals, according to a 2018 report by the New York Times.
After the meeting, Ye sent Hunter a 2.8-carat diamond and a “thank you” note and the ex-veep’s son began negotiating a deal for CEFC to invest $40 million dollars in a natural gas project on Louisiana’s Monkey Island.

![1 million a year](https://nypost.com/wp-content/uploads/sites/2/2020/10/Biden-Patrick-Ho-Agreement.jpg?quality=90&strip=all&w=662)

Patrick Ho (in the email above, paying Hunter 1 million), was arrested in late 2017 by federal agents in New York for money laundering 
and violating the Foreign Corrupt Practices Act. 
Ho was an assistant to Chinese oil tycoon Ye Jianming, who ran the energy company CEFC. 

Hunter Biden would later admit Ye sent him a “large diamond” after one of their meetings. 
In March 2019, Ho was jailed in the U.S. on bribery charges stemming from oil deals in Africa. 
According to the New York Times, it appears that Ho’s first phone call while in the custody of U.S. officials was an attempt to reach Hunter Biden.

CEFC officials had met multiple times with Hunter Biden beginning back in 2015. In May
2017, Hunter met with Ho in Miami, where they reportedly discussed joint energy and
infrastructure deals in the United States. The night of his arrest, Ho called James Biden,
Joe’s brother. According to James Biden,
the call was a surprise; he believed Ho was trying to contact Hunter so he gave him his
nephew’s number. 
During Ho’s trial, prosecutors presented evidence that he was an arms dealer on the side,
running guns to conflict zones around the world. He was sentenced to three years in jail.

