# How do we know the laptop is real?

## Email chains

Many emails from the laptop have been independently verified by the people in these email chains.
After the initial leaks, Hunter Biden's ex business partner Tony Bobulinski has come forward, bringing [three of his phones](https://www.foxnews.com/media/cnn-msnbc-skip-tony-bobulinski-statement-joe-biden) to the FBI to help with the investigation.

Bevan Cooney, ex business partner of Hunter Biden, currently in prison, has also flipped and come forward with his side of the emails. 
[He has since been moved to a separate jail call](https://www.breitbart.com/politics/2020/10/20/exclusive-bevan-cooney-moved-from-prison-cell-after-providing-email-account-exposing-hunter-biden/).

## Photographic evidence

The laptop contains a thousands of private pictures and videos depicting Hunter biden, for example:

- [Hunter Biden getting a footjob while smoking crack](https://gtv.org/web/?videoid=5f94837c7de25667c0fe0c5e#/VideoPlay_UI)
- [Hunter Biden footjob #2](https://gtv.org//?videoid=5f97af07e7d0c633de14b319)
- [Hunter Biden with Barack Obama](https://gnews.org/451659/)
